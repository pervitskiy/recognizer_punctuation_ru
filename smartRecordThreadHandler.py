import time
from PyQt5 import QtCore
import speech_recognition
import transformers
import os

from punctuation import Punctuation


class SmartRecordThreadHandler(QtCore.QThread):
    signal = QtCore.pyqtSignal(list)
    handler_status = True
    model = transformers.AutoModelForTokenClassification.from_pretrained('model').to('cpu')
    # tokenizer = transformers.AutoTokenizer.from_pretrained('DeepPavlov/rubert-base-cased')
    tokenizer = transformers.AutoTokenizer.from_pretrained('DeepPavlov/rubert-base-cased-sentence')
    text_result = ''
    text_with_punctuation = ''
    os.environ["TOKENIZERS_PARALLELISM"] = "false"

    # Работает в отдельном потоке
    def callback(self, rc, audio):
        if self.handler_status:
            try:
                voice = rc.recognize_google(audio, language="ru-RU").lower()
                # print(voice)
                voice += ' '
                self.text_result += voice
                self.text_with_punctuation = Punctuation.make_punct(self.text_result, self.model, self.tokenizer)
                # print(self.text_with_punctuation[0])
                self.signal.emit(['response', self.text_with_punctuation])
            except speech_recognition.UnknownValueError:
                print("[log] Голос не распознан!")
            except speech_recognition.RequestError as e:
                print("[log] Неизвестная ошибка, проверьте интернет!")
        else:
            self.text_result = ''

    def run(self):
        self.signal.emit(['start_thread'])
        try:
            if self.handler_status:
                self.record_object = speech_recognition.Recognizer()
                with speech_recognition.Microphone() as source:
                    self.record_object.adjust_for_ambient_noise(source, duration=0.5)
                    # self.record_object.listen(source)
                self.record_object.listen_in_background(source, self.callback, 30)
                # time.sleep(0.1)
            else:
                return
        except Exception as err:
            self.signal.emit(['response', "Не удалось найти микрофон"])
            self.signal.emit(['exit'])
            return

