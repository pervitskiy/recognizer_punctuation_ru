import transformers
import torch

PUNCTUATION = '!,-.:;?()'


def char_to_id(s):
    result = {}
    for i, c in enumerate(s):
        result[c] = i
    return result


PUNCT_TO_ID = char_to_id(' ' + PUNCTUATION)


class Punctuation:

    @staticmethod
    def format(text):
        new_text = text.replace('  ', ' ')\
            .replace(' ,', ',')\
            .replace(' .', '.')\
            .replace(' :', ':')\
            .replace(' ?', '?')\
            .replace(' !', '!')\
            .replace('  - -  ', ' - ')\
            .replace(' - - ', ' - ')

        for i, char in enumerate(new_text):
            if char == '.' or char == '?' or char == '!':
                try:
                    upper_char = new_text[i + 2].upper()
                    new_text = new_text[:i + 2]+upper_char+new_text[i+3:]
                except:
                    pass
        upper_char = new_text[0].upper()
        new_text = new_text[:0] + upper_char + new_text[1:]

        return new_text

    @staticmethod
    def restore_punct(token_ids, tokenizer, token_label_ids):
        result = ''
        for token_id, label_id in list(zip(token_ids, token_label_ids)):
            token_id = token_id.item()
            if token_id == tokenizer.cls_token_id:
                continue
            if token_id in (tokenizer.sep_token_id, tokenizer.pad_token_id):
                break
            token = tokenizer.convert_ids_to_tokens(token_id)
            if token.startswith('##'):
                result = result[:-2]  # remove last added punctuation
                token = token[2:]
            elif len(result) > 0 and result[-1] != ' ':
                result += ' '

            result += token
            result += ' ' + (' ' + PUNCTUATION)[label_id]
        return result

    @staticmethod
    def make_punct(texts, model, tokenizer):
        encoded = tokenizer(texts, max_length=192, padding="max_length", truncation=True, return_tensors='pt')
        encoded = {k: v.cpu() for k, v in encoded.items()}
        with torch.no_grad():
            model_out = model(**encoded)
        predicted_tokens = torch.argmax(model_out[0], dim=-1)
        results = []
        for sample_ids, sample_predicts in list(zip(encoded['input_ids'], predicted_tokens)):
            result = Punctuation.restore_punct(sample_ids, tokenizer, sample_predicts)

            results.append(result)
        return Punctuation.format(results[0])
