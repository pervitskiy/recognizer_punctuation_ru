from PyQt5.QtWidgets import QApplication
from smartRecordThreadHandler import SmartRecordThreadHandler
from graphic_interface import *
from PyQt5 import QtGui, QtCore, QtWidgets
import speech_recognition  # распознавание пользовательской речи (Speech-To-Text)
import os  # работа с файловой системой
import keyboard
import sys


class Interface(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.isAlive = False

        # Убираем рамки и разрешаем перетаскивание объектов
        self.setWindowFlag(QtCore.Qt.FramelessWindowHint)
        self.setAttribute(QtCore.Qt.WA_TranslucentBackground)
        self.center()

        # Обработчик основной панели
        self.ui.pushButton_3.clicked.connect(lambda: self.close())
        self.ui.pushButton_4.clicked.connect(lambda: self.showMinimized())
        self.ui.pushButton.clicked.connect(self.check_microphone)
        self.ui.pushButton_2.clicked.connect(self.start_smart_record)

        # Запуск распознования голоса в отдельном потоке
        self.thread_handler = SmartRecordThreadHandler()
        self.thread_handler.signal.connect(self.signal_handler)

    # Центровка безрамочного окна и его перетаскивание
    def center(self):
        qr = self.frameGeometry()
        cp = QtWidgets.QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def mousePressEvent(self, event):
        self.oldPos = event.globalPos()

    def mouseMoveEvent(self, event):
        try:
            delta = QtCore.QPoint(event.globalPos() - self.oldPos)
            self.move(self.x() + delta.x(), self.y() + delta.y())
            self.oldPos = event.globalPos()
        except AttributeError:
            pass

    # Получить все микрофоны в системе
    def check_microphone(self):
        microphone_list = microphone.list_microphone_names()
        if len(microphone_list) == 0:
            microphone_list = 'Микрофоны не обнаружены в системе'
        QtWidgets.QMessageBox.about(self, 'Микрофоны в системе ', str(microphone_list))

    def start_smart_record(self):
        if self.ui.label_2.text().lower() == "офлайн":
            self.signal_handler(['start_thread'])
            self.thread_handler.handler_status = True
            if not self.isAlive:
                self.thread_handler.start()
                self.isAlive = True

        elif self.ui.label_2.text().lower() == "онлайн":
            self.signal_handler(['exit'])
            self.thread_handler.handler_status = False
            self.thread_handler.exec()

    def signal_handler(self, value: list) -> None:
        if value[0] == 'start_thread':
            self.ui.plainTextEdit.clear()
            self.ui.label_2.setText("Онлайн")
            self.ui.pushButton_2.setText("Стоп")
            self.ui.plainTextEdit.appendPlainText("Говорите...")

        elif value[0] == 'response':
            self.ui.plainTextEdit.clear()
            self.ui.plainTextEdit.appendPlainText(value[1])

        elif value[0] == 'exit':
            self.ui.label_2.setText("Офлайн")
            self.ui.pushButton_2.setText("Начать голосовой ввод")

    def minimized(self):
        self.showMinimized()

    def normal(self):
        self.showNormal()

    def copyText(self):
        clipboard = QApplication.clipboard()
        if clipboard is not None:
            clipboard.setText(self.ui.plainTextEdit.toPlainText())


if __name__ == '__main__':
    # инициализация инструментов распознавания и ввода речи
    recognizer = speech_recognition.Recognizer()
    microphone = speech_recognition.Microphone()  # device_index = индекс номера микрофона

    app = QtWidgets.QApplication(sys.argv)
    win = Interface()
    win.show()
    #
    # keyboard.add_hotkey('1', win.minimized()) - свернуть окно -- баг
    # на стороне qt, может работать, а может и нет, зависит от версии

    # keyboard.add_hotkey('2', win.normal()) развернуть окно -- баг на стороне qt, может работать,
    # а может и нет, зависит от версии

    # можно заменить на любую комбинацию
    # https://chel-center.ru/python-yfc/2021/03/02/modul-keyboard-upravlenie-klaviaturoj-v-python/

    keyboard.add_hotkey('3', win.start_smart_record)
    keyboard.add_hotkey('4', win.copyText)
    sys.exit(app.exec_())
    keyboard.wait("esc")

